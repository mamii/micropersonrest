package mami.micropersonrest.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mami.micropersonrest.domain.Person;

/**
 *
 * @author maierm
 */
public class PersonRepository {

    public static final PersonRepository INSTANCE = new PersonRepository();

    static final Map<Integer, Person> id2Person = new HashMap<>();
    private static final int INITIAL_COUNT = 1000;

    static {
        for (int i = 0; i < INITIAL_COUNT; i++) {
            id2Person.put(i, new Person(i, "fn" + i, "ln" + i));
        }
    }

    public List<Person> findAll() {
        return new ArrayList<>(id2Person.values());
    }

    public Person findById(Integer id) {
        return id2Person.get(id);
    }
}
