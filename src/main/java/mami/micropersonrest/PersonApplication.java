package mami.micropersonrest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/api")
public class PersonApplication extends Application {

    private final Set<Object> singletons = new HashSet<Object>();

    public PersonApplication() {
        singletons.add(new PersonREST());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
