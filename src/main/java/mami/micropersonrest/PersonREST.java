package mami.micropersonrest;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import mami.micropersonrest.domain.Person;
import mami.micropersonrest.repository.PersonRepository;

@Path("persons")
public class PersonREST {

    private PersonRepository getRepo() {
        return PersonRepository.INSTANCE;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Person> getAll() {
        return getRepo().findAll();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Person getById(@PathParam("id") Integer id) {
        Person result = getRepo().findById(id);
        if (result == null) {
            throw new NotFoundException();
        }
        return result;
    }

}
